/*********************************************************************
 * Copyright (c) 2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

#ifndef LOCK_HH_
#define	LOCK_HH_

#include <pthread.h>

/**
 * A convenience wrapper around a mutex that unlocks the mutex when the
 * instance goes out of scope.
 */
class LockGuard {
public:
  /**
   * Constructor taking a reference to the mutex that should be released in
   * destructor.
   */
  explicit LockGuard(pthread_mutex_t &mutex);

  /**
   * Destructor.
   */
  ~LockGuard();

  LockGuard(const LockGuard &) = delete;

  LockGuard(LockGuard&&) = default;
private:
  pthread_mutex_t &mutex;
};

/**
 * A convenience wrapper around a pthread mutex supporting automatic
 * destruction via RAII and postponed unlocking.
 */
class Lock {
public:
  /**
   * Default constructor.
   */
  Lock();

  /**
   * Destructor.
   */
  ~Lock();

  Lock(const Lock &) = delete;

  /**
   * Lock the underlying mutex.
   * This function can be safely called multiple times by the same thread.
   */
  void lock();

  /**
   * Unlock the underlying mutex.
   * The mutex will be lockable by other threads once the number of `lock' and
   * `unlock' calls match.
   */
  void unlock();

  /**
   * A modified version of the `unlock' function that will postpone the unlock
   * until the end of the current scope is reached.
   * The mutex will be unlocked even if an exception is thrown.
   */
  LockGuard unlock_at_exit();
private:
  pthread_mutex_t mutex;
};

#endif
