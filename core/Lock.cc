/*********************************************************************
 * Copyright (c) 2021 ITK Engineering GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

#include <Lock.hh>

LockGuard::LockGuard(pthread_mutex_t &mutex): mutex(mutex) {}

LockGuard::~LockGuard() {
  pthread_mutex_unlock(&this->mutex);
}

Lock::Lock() {
  pthread_mutexattr_t attr;
  pthread_mutexattr_init(&attr);
  pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init(&this->mutex, &attr);
}

Lock::~Lock() {
  pthread_mutex_destroy(&this->mutex);
}

void Lock::lock() {
  pthread_mutex_lock(&this->mutex);
}

void Lock::unlock() {
  pthread_mutex_unlock(&this->mutex);
}

LockGuard Lock::unlock_at_exit() {
    return LockGuard(this->mutex);
}
